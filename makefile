all: fat16

fat16:	fat16.o
	gcc obj/fat16.o -o bin/testeme

fat16.o:
	gcc -c src/fat16.c -o obj/fat16.o

clean:
	rm obj/*.* bin/testeme
