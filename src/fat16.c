#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma pack(push, 1)
typedef struct
{
	unsigned char jmp[3]; //[0]
	char oem[8]; //[3]
	unsigned short sector_size; //[11]
	unsigned char sectors_per_cluster; //[13]
	unsigned short reserved_sectors; //[14]
	unsigned char number_of_fats; //[16]
	unsigned short root_dir_entries; //[17]
	unsigned short total_sectors_short; //[19]
	unsigned char media_descriptor; //[21]
	unsigned short fat_size_sectors; //[22]
	unsigned short sectors_per_track; //[24]
	unsigned short number_of_heads; //[26]
	//Antes disso é igual ao fat12
	unsigned int hidden_sectors; //[28]
	unsigned int total_sectors_long; //[32]
	unsigned char drive_number; //[36]
	unsigned char current_head; //[37]
	unsigned char boot_signature; //[38]
	unsigned int volume_id; //[39]
	char volume_label[11]; //[43]
	char fs_type[8]; //[54]
	char boot_code[448]; //[62] bootstrap
    unsigned short boot_sector_signature; //[510]
} struct_fat16_boot;

typedef struct {
    /* filename[0]
     * 0x00 : arquivo nunca usado
     * 0xE5 : arquivo usado, porém deletado
     * 0x05 : primeiro carácter do nome do arquivo é 0xE5
     * 0x2E : diretório
     */
    unsigned char filename[8];
    unsigned char ext[3];
    unsigned char attributes;
    unsigned char reserved[10];
    unsigned short modify_time;
    unsigned short modify_date;
    unsigned short starting_cluster;
    unsigned int file_size;
} struct_fat16_entrada;

typedef struct
{
	FILE				*fp;
	struct_fat16_boot	bs;//setor de boot
} struct_auxiliar;

#pragma pack(pop)

static char					blocos[0xFFFF];

void err_open()
{
	fprintf(stderr, "Erro ao tentar abrir o arquivo.\n");
	exit(-1);
}

void err_alloc()
{
	fprintf(stderr, "Erro ao tentar alocar espaço.\n");
	exit(-1);
}

void err_args()
{
	fprintf(stderr, "Argumentos incorretos. Uso: fat16 [opção] [arquivo]\n");
	exit(-1);
}

//Lê a fat desejada e retorna um vetor de shorts contendo a mesma
unsigned short *le_fat(struct_auxiliar *fat, int n)
{
	unsigned short	*fat_vet	= calloc(fat->bs.fat_size_sectors, fat->bs.sector_size); //Aloca memória para armazenar a fat
	int 			ini_fat, off_fat;
	if( !fat_vet )
	{
		free(fat);
		err_alloc();
	}
	ini_fat = sizeof(struct_fat16_boot) + ((fat->bs.reserved_sectors-1) * fat->bs.sector_size);//offset para o início das fats
	off_fat = (ini_fat + (fat->bs.fat_size_sectors * fat->bs.sector_size * n));//offset para a fat desejada
	fseek(fat->fp, off_fat, SEEK_SET);//Coloca o ponteiro do arquivo aberto na posição inicial da fat
	fread(fat_vet, fat->bs.fat_size_sectors, fat->bs.sector_size, fat->fp);//Efetua a leitura da fat
	return fat_vet;
}

//Realiza a abertura do arquivo e leitura do setor de boot
struct_auxiliar *open_fat(char *file)
{
	struct_auxiliar *aux = malloc(sizeof(struct_auxiliar));
	if( !aux ) err_alloc();
	aux->fp = fopen(file, "rb");
	if( !aux->fp ) err_open();
	fread(&aux->bs, sizeof(struct_fat16_boot), 1, aux->fp);//Lê setor de boot da fat
	return aux;
}

//Lista blocos que diferem entre uma fat e outra
void cmp_fat(char *file)
{
	struct_auxiliar		*fat		= open_fat(file);
	unsigned short		*ptr_fat_1	= le_fat(fat, 0), *ptr_fat_2	= le_fat(fat, 1), *ptr_1, *ptr_2;
	int					i;
	for(i = 2, ptr_1 = ptr_fat_1+2, ptr_2 = ptr_fat_2+2; i < (fat->bs.fat_size_sectors * fat->bs.sector_size) / sizeof(short); i++, ptr_1++, ptr_2++)//Dois primeiros blocos são reservados
	{
		if( *ptr_1 != *ptr_2 )
		{
			printf("DIF %d:%d,%d\n", i, *ptr_1, *ptr_2);
		}
	}
	fclose(fat->fp);
	free(fat);
	free(ptr_fat_1);
	free(ptr_fat_2);
}

//Lista todos os blocos livres
void lista_livres(char *file)
{
	struct_auxiliar		*fat		= open_fat(file);
	unsigned short		*ptr_fat	= le_fat(fat, 0), *ptr;
	int					i;
	char				primeiro = 0;
	printf("LIVRE");
	for(i = 2, ptr = ptr_fat+2; i < (fat->bs.fat_size_sectors * fat->bs.sector_size) / sizeof(short); i++, ptr++)//Dois primeiros blocos são reservados
		if( *ptr == 0 )
		{
			if( primeiro )
				printf(",%d", i);
			else
			{
				primeiro = 1;
				printf(" %d", i);
			}
		}
	printf("\n");
	fclose(fat->fp);
	free(fat);
	free(ptr_fat);
}

//Lista todos os blocos livres com dados
void lista_ocupados(char *file)
{
	struct_auxiliar		*fat		= open_fat(file);
	unsigned short		*ptr_fat	= le_fat(fat, 0), *ptr;
	int					i, j;
	char				primeiro 	= 0;
	int 				ini_fat, dados_fat;
	int					*cluster;
	cluster = malloc(fat->bs.sectors_per_cluster * fat->bs.sector_size);//Aloca espaço para o cluster
	if( !cluster ) err_alloc();
	ini_fat = sizeof(struct_fat16_boot) + ((fat->bs.reserved_sectors-1) * fat->bs.sector_size);//offset para o início das fats
	dados_fat = (ini_fat + (fat->bs.fat_size_sectors * fat->bs.sector_size * 2) + fat->bs.root_dir_entries * sizeof(struct_fat16_entrada));//offset para o início dos dados
	struct_fat16_entrada	entrada;
	printf("REMOVIDOS");
	for(i = 2, ptr = ptr_fat+2; i < (fat->bs.fat_size_sectors * fat->bs.sector_size) / sizeof(short); i++, ptr++)//Dois primeiros blocos são reservados
	{
		if( *ptr == 0 )
		{
			fseek(fat->fp, dados_fat + ((i-2) * fat->bs.sectors_per_cluster * fat->bs.sector_size), SEEK_SET);//Posiciona o ponteiro do arquivo no cluster que deseja verificar
			fread(cluster, fat->bs.sectors_per_cluster, fat->bs.sector_size, fat->fp);
			for(j = 0; j < (fat->bs.sectors_per_cluster * fat->bs.sector_size / 4); j++)
				if( cluster[j] )
				{
					if( primeiro )
					{
						printf(",%d", i);
					}else
					{
						printf(" %d", i);
						primeiro = 1;
					}
					break;
				}
		}
	}
	printf("\n");
	fclose(fat->fp);
	free(fat);
	free(cluster);
	free(ptr_fat);
}

void copia(char *file, int origem, int destino)
{
	struct_auxiliar		*fat		= open_fat(file);
	unsigned short		*ptr_fat	= le_fat(fat, origem);
	int					ini_fat, off_fat;
	ini_fat = sizeof(struct_fat16_boot) + ((fat->bs.reserved_sectors-1) * fat->bs.sector_size);//offset para o início das fats
	off_fat = (ini_fat + (fat->bs.fat_size_sectors * fat->bs.sector_size * destino));//offset para a fat desejada
	fclose(fat->fp);
	fat->fp = fopen(file, "r+");
	fseek(fat->fp, off_fat, SEEK_SET);//Coloca o ponteiro do arquivo aberto na posição inicial da fat destino
	fwrite(ptr_fat, fat->bs.fat_size_sectors, fat->bs.sector_size, fat->fp);//Grava todo o vetor lido na fat de origem na posição da fat de destino
	fclose(fat->fp);
	free(fat);
	free(ptr_fat);
}

int main(int argc, char **argv)
{
	if( argc < 3 || argv[1][0] != '-' )
		err_args();
	switch( argv[1][1]+argv[1][2]+argv[1][3] )
	{
		case 'v'+'f':
			cmp_fat(argv[2]);
		break;
		case 'b'+'l':
			lista_livres(argv[2]);
		break;
		case 'b'+'d':
			lista_ocupados(argv[2]);
		break;
		case 'c'+'f'+'1':
			copia(argv[2], 1, 0);
		break;
		case 'c'+'f'+'2':
			copia(argv[1], 0, 1);
		break;
		default:
			err_args();
	}
	return 0;
}
